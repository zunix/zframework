package com.zunix.zframework

import android.content.Context
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.zunix.zframework.patterns.ContextSingleton
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Crée par cyril le 22/03/2020.
 */
@RunWith(AndroidJUnit4::class)
class ContextSingletonInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext

        val instanceA = TestSingleton.getInstanceWithContext(appContext)
        val instanceB = TestSingleton.getInstanceWithContext(appContext)
        Assert.assertEquals(instanceB,instanceA)
    }

    class TestSingleton(private val context: Context) {
        companion object : ContextSingleton<TestSingleton>(::TestSingleton)
    }
}