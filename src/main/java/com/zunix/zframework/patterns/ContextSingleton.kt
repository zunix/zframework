package com.zunix.zframework.patterns

import android.content.Context

/**
 * Singleton utilisant un contexte Android. Thread safe.
 * Utilisable via
 * <pre>
 * {@code
 *  class Exemple private constructor(context: Context) {
 *      companion object : ContextSingleton<Exemple>(::Exemple)
 *  }
 * }
 * </pre>
 */
open class ContextSingleton <out T: Any>(private val creator: (Context) -> T) {

    @Volatile
    private var instance: T? = null

    /**
     * Obtenir l'instance Singleton de la classe avec le [androidContext] choisi
     * @param androidContext contexte android avec lequel utiliser la classe
     * @return Singleton de la classe avec le [androidContext] choisi
     */
    fun getInstanceWithContext(androidContext: Context): T {
        return instance ?: createSyncronizedInstance(androidContext)
    }

    /**
     * Créer une instance en respectant la synchro des threads
     * @param androidContext contexte android avec lequel utiliser la classe
     */
    private fun createSyncronizedInstance(androidContext: Context): T = synchronized(this) {
        var currentInstance = instance
        if (currentInstance == null) {
            currentInstance= creator(androidContext)
            instance = currentInstance
        }
        return@synchronized currentInstance
    }
}